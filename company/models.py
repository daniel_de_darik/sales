from django.db import models

class Company(models.Model):
	title = models.CharField(max_length=100)
	description = models.TextField()
	image = models.CharField(max_length=20)
	created_date = models.DateTimeField(auto_now=True)
	address = models.CharField(max_length=100)

	def __unicode__(self):
		return self.title
