from django.views.generic import TemplateView
from category.views import CategoryMixin

class AboutView(CategoryMixin, TemplateView):
	template_name = 'about.html'

	def get_context_data(self, **kwargs):
		context = super(AboutView, self).get_context_data(**kwargs)
		context['category_list'] = self.get_header_category()

		return context;

class ContactsView(CategoryMixin, TemplateView):
	template_name = 'contacts.html'

	def get_context_data(self, **kwargs):
		context = super(ContactsView, self).get_context_data(**kwargs)
		context['category_list'] = self.get_header_category()

		return context;
