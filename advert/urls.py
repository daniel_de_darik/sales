from django.conf.urls import patterns, url
from advert import views
from django.contrib.auth.decorators import user_passes_test

urlpatterns = patterns('',
	url(r'^$', views.AdvertListView.as_view(), name='adds-index'),
	url(r'^create/$', views.AdvertCreateView.as_view(), name='adds-create'),
	# url(r'^(?P<category_name>\w+)/$', views.category, name='adds-category'),
	url(r'^(?P<category_name>\w+)/$', views.AdvertListView.as_view(), name='adds-category'),
	url(r'^loadmore/(?P<category_id>\d+)/(?P<page>\d+)/$', views.loadmore, name='adds-load'),
)