from django.db import models
from company.models import Company
from category.models import Category

# Create your models here.
class Advert(models.Model):
	title = models.CharField(max_length=100)
	description = models.TextField()
	address = models.CharField(max_length=100)
	start_date = models.DateTimeField()
	expire_date = models.DateTimeField()
	created_date = models.DateTimeField(auto_now=True)
	image = models.ImageField(upload_to='img')
	company = models.ForeignKey(Company)
	category = models.ForeignKey(Category)

	def __unicode__(self):
		return self.title