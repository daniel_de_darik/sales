from django import forms
from advert.models import Advert
from django.conf import settings
from django.core.files.storage import FileSystemStorage
from django.core.files.base import ContentFile
import string
import random
import Image
# import os

THUBNAIL_SIZE = (300, 300)
ORIGINAL_SIZE = (800, 800)

class AdvertCreateForm(forms.ModelForm):
	class Meta:
		model = Advert

	def clean_image(self):
		max_size = 10**6
		if len(self.cleaned_data['image'].read()) > max_size:
			raise forms.ValidationError('Images must be less than %d bytes' % max_size)

		return self.cleaned_data['image']

	def save(self, *args, **kwargs):
		advert = super(AdvertCreateForm, self).save(*args, **kwargs)

		EXT = settings.EXT
		filename = ''.join(random.choice(string.ascii_lowercase) for i in range(10))
		original = filename + '-original' + EXT

		prefix = filename[:2]
		# destination = os.path.join(settings.MEDIA_ROOT, prefix)
		destination = settings.MEDIA_ROOT + '/' + prefix + '/'

		storage = FileSystemStorage(location=destination, base_url='')
		image = self.cleaned_data['image']
		storage.save(original, image)

		try:
			im = Image.open(destination + original)
			im.thumbnail(ORIGINAL_SIZE, Image.ANTIALIAS)
			im.save(destination + original, 'JPEG')
		except IOError:
			pass

		try:
			im = Image.open(destination + original)
			im.thumbnail(THUBNAIL_SIZE, Image.ANTIALIAS)
			im.save(destination + filename + EXT, 'JPEG')
		except IOError:
			pass


		advert.image = filename
		advert.save()

		return advert
