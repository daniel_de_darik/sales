from django.views.generic.list import ListView
from django.views.generic.edit import CreateView
from django.template import RequestContext
from django.shortcuts import render_to_response, get_object_or_404, redirect
from django.core.urlresolvers import reverse
from django.http import Http404
from advert.models import Advert
from advert.forms import AdvertCreateForm
from category.models import Category
from category.views import CategoryMixin
from django.conf import settings

PER_PAGE = 10

def process(adv_list):
	for adv in adv_list:
		image = str(adv.image)
		adv.image = image[:2] + '/' + image + settings.EXT
	return adv_list	

class AdvertListView(CategoryMixin, ListView):
	model = Advert
	context_object_name = 'adv_list'
	template_name = 'advert/list.html'

	def get_queryset(self):
		category_name = self.kwargs.get('category_name', '')
		category_name = category_name.replace('_', ' ').lower()

		adv_list = []
		if not category_name:
			adv_list = Advert.objects.order_by('-created_date')[:PER_PAGE]
		else:
			category = Category.objects.filter(title__iexact=category_name)
			adv_list = Advert.objects.filter(category=category).order_by('-created_date')[:PER_PAGE]

		return process(adv_list)

	def get_context_data(self, *args, **kwargs):
		context = super(AdvertListView, self).get_context_data(*args, **kwargs)
		context['category_list'] = self.get_header_category()
		return context

class AdvertCreateView(CreateView):
	form_class = AdvertCreateForm
	template_name = 'advert/create.html'
	success_url	= '/adv/'

# def category(request, category_name):
# 	context = RequestContext(request)
# 	category_name = category_name.replace('_', ' ').lower()
# 	category = None

# 	category_list = Category.objects.all()
# 	for ctg in category_list:
# 		if ctg.title.lower() == category_name:
# 			category = ctg
# 			break
	
# 	if category is None:
# 		return redirect(reverse('advert:adds-index'))
	
# 	adv_list = Advert.objects.filter(category=category).order_by('-created_date')[:PER_PAGE]
	
# 	return render_to_response('advert/list.html', {'adv_list' : adv_list, 
# 												  'category_list' : category_list,
# 												  'category' : category }, context)

def loadmore(request, category_id, page):
	context = RequestContext(request)
	
	page = int(page)
	if page <=0: page = 1
	offset=(page-1)*PER_PAGE

	category = None
	try:
		category = Category.objects.get(pk=int(category_id))
	except Category.DoesNotExist:
		pass

	adv_list = Advert.objects.order_by('-created_date')
	if category is not None:
		adv_list = adv_list.filter(category=category)

	adv_list = adv_list[offset:PER_PAGE]
	adv_list = process(adv_list)
	
	return render_to_response('advert/enumerate.html', {'adv_list' : adv_list}, context)
