(function($){
	var $container = $('#container'),
		page = 1,
        category_id = $('#category_id').val(),
		cache = {};
	
	$container.isotope({
		itemSelector: '.adv-item',
		masonryHorizontal: {
			rowHeight: 900
		}
	});
	
	$(window).scroll(function(){

        var wintop = $(window).scrollTop(), docheight = $(document).height(), winheight = $(window).height();
        var  scrolltrigger = 0.95;

        if  ((wintop/(docheight-winheight)) > scrolltrigger) {
         	loadMore();
        }
    });

    function loadMore() {
    	$('.more-loader').html('<img src="/static/img/loader.gif">');
    	if (page in cache) {
    		$('.more-loader').empty();
    		return;
    	}
    	cache[page] = 1;
    	$.get("/adv/loadmore/" + category_id + "/" + (page), function(data){
            if (data != "") {
            	var $data = $(data);
                $container.isotope('insert', $data);
                page++;
            }
            $('.more-loader').empty();
        });
    }

})(jQuery);