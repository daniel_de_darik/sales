from django.conf.urls import patterns, include, url
from django.conf import settings
from company import views

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'sales.views.home', name='home'),
    url(r'^adv/', include('advert.urls', namespace='advert')),
    url(r'^about/', views.AboutView.as_view(), name='about'),
    url(r'^contacts/', views.ContactsView.as_view(), name='contacts'),
    url(r'^media/(?P<path>.*)$','django.views.static.serve',{'document_root': settings.MEDIA_ROOT}),
    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
)

# if settings.DEBUG:
#     urlpatterns += patterns(
#             'django.views.static',
#             (r'media/(?P<path>.*)',
#             'serve',
#             {'document_root': settings.MEDIA_ROOT}), )