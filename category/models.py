from django.db import models

class Category(models.Model):
	title = models.CharField(max_length=64)
	created_date = models.DateTimeField(auto_now=True)

	def __unicode__(self):
		return self.title
