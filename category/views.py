from category.models import Category

class CategoryMixin(object):
	def get_header_category(self, **kwargs):
		category_list = Category.objects.all()
		for category in category_list:
			category.url = category.title.replace(' ', '_')

		return category_list